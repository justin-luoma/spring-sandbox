package dev.luoma.testing.common;

import dev.luoma.testing.events.MessageAdapterListener;
import dev.luoma.testing.common.ro.ItemRO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@RestController
@RequestMapping(TestDataController.PATH)
public class TestDataController {
  static final String PATH = "test-data";

  private final MessageAdapterListener messageAdapterListener;

  @Autowired
  public TestDataController(MessageAdapterListener messageAdapterListener) {
    this.messageAdapterListener = messageAdapterListener;
  }

  @GetMapping
  @CrossOrigin(origins = "http://localhost:3000")
  public ResponseEntity<List<ItemRO>> getTestData() throws InterruptedException {
    Thread.sleep(5000);
    List<ItemRO> itemROS = IntStream.range(1, 11)
        .mapToObj(
            i -> new ItemRO(i, String.valueOf(i))
        )
        .collect(Collectors.toList());
    return ResponseEntity.ok(itemROS);
  }

  @GetMapping("/stream")
  @CrossOrigin(origins = "http://localhost:3000")
  public Flux<ServerSentEvent<ItemRO>> streamTestData() {

    return messageAdapterListener.subscribe();
  }
}
