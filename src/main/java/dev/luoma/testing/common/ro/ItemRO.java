package dev.luoma.testing.common.ro;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class ItemRO {
  private long id;
  private String label;

  public ItemRO(long id, String label) {
    this.id = id;
    this.label = label;
  }
}
