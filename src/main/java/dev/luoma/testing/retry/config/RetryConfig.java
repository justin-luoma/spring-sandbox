package dev.luoma.testing.retry.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.support.RetryTemplate;

@EnableRetry
@Configuration
public class RetryConfig {

  private final RetryListener retryListener;

  @Autowired
  public RetryConfig(RetryListener retryListener) {
    this.retryListener = retryListener;
  }

  @Bean
  public RetryTemplate retryTemplate() {
    RetryTemplate retryTemplate = new RetryTemplate();

    retryTemplate.registerListener(retryListener);

    return retryTemplate;
  }
}
