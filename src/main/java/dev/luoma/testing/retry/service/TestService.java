package dev.luoma.testing.retry.service;

import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public interface TestService {
  @Retryable(value = RuntimeException.class)
  @Async
  CompletableFuture<String> getTestData();
}
