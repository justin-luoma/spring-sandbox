package dev.luoma.testing.retry.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class TestRestService implements TestService {

  public static final String URL = "http://localhost:8181/v1/testData";

  private final RestTemplate restTemplate;

  @Autowired
  public TestRestService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public CompletableFuture<String> getTestData() {

    String resp = restTemplate.getForObject(URL, String.class);

    return CompletableFuture.completedFuture(resp);
  }
}
