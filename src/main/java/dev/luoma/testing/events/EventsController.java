package dev.luoma.testing.events;

import dev.luoma.testing.events.publisher.MessageEventPublisher;
import dev.luoma.testing.events.ro.MessageRO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(EventsController.PATH)
public class EventsController {
  static final String PATH = "events";

  private final MessageEventPublisher messagePublisher;

  @Autowired
  public EventsController(MessageEventPublisher messagePublisher) {
    this.messagePublisher = messagePublisher;
  }

  @PostMapping
//  @CrossOrigin(origins = "http://localhost:3000")
  public ResponseEntity<Object> createMessage(@RequestBody MessageRO messageRO) {
    log.info("creating message: {}", messageRO);

    messagePublisher.publishMessage(messageRO);

    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @GetMapping
  public ResponseEntity<MessageRO> testMessage() {
    log.info("sending test message");

    MessageRO messageRO = new MessageRO();
    messageRO.setMessage("DEBUG");

    return ResponseEntity.ok(messageRO);
  }
}
