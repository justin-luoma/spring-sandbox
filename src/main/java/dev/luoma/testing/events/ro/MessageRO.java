package dev.luoma.testing.events.ro;

import lombok.Data;

@Data
public class MessageRO {
  private String message;
}
