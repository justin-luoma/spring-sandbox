package dev.luoma.testing.events.event;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

@Getter
@ToString
public class MessageEvent extends ApplicationEvent {
  private final String message;
  private final long id;

  /**
   * Create a new {@code ApplicationEvent}.
   *
   * @param source the object on which the event initially occurred or with
   *               which the event is associated (never {@code null})
   * @param id
   */
  public MessageEvent(Object source, String message, long id) {
    super(source);
    this.message = message;
    this.id = id;
  }
}
