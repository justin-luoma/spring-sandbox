package dev.luoma.testing.events;

import dev.luoma.testing.events.event.MessageEvent;
import dev.luoma.testing.common.ro.ItemRO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Service
public class MessageAdapterListener implements ApplicationListener<MessageEvent> {
  private final AtomicLong messageId = new AtomicLong(1);
  private final Sinks.Many<ServerSentEvent<ItemRO>> sink;

  public MessageAdapterListener() {
    sink = Sinks.many().multicast().onBackpressureBuffer();
  }

  @PostConstruct
  private void fluxTicker() {
    log.info("fluxTicker");
    Flux.interval(Duration.ofSeconds(5L)).map(n -> ServerSentEvent.<ItemRO>builder()
    .id(String.valueOf(n))
    .data(new ItemRO(messageId.getAndIncrement(), "tick-" + n))
    .build()).subscribe(itemROServerSentEvent -> sink.emitNext(itemROServerSentEvent, (signalType, emitResult) -> true));
  }

  public Flux<ServerSentEvent<ItemRO>> subscribe() {
    return sink.asFlux();
  }

  @Override
  public void onApplicationEvent(MessageEvent event) {
    log.info("messageEvent: {}", event);
    Sinks.EmitResult result = sink.tryEmitNext(
        ServerSentEvent.<ItemRO>builder()
            .id(String.valueOf(messageId.get()))
            .data(new ItemRO(messageId.getAndIncrement(), event.getMessage()))
        .build()
    );
    if (result.isFailure()) {
      log.error("failed to send message");
    }
  }
}
