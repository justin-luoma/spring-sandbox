package dev.luoma.testing.events.publisher;

import dev.luoma.testing.events.ro.MessageRO;

public interface MessageEventPublisher {
  void publishMessage(final MessageRO message);
}
