package dev.luoma.testing.events.publisher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.luoma.testing.events.event.MessageEvent;
import dev.luoma.testing.events.ro.MessageRO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Component
public class MessageEventPublisherImpl implements MessageEventPublisher {

  private final ApplicationEventPublisher eventPublisher;
  private final ObjectMapper objectMapper;

  private AtomicLong messageId = new AtomicLong(1);

  @Autowired
  public MessageEventPublisherImpl(ApplicationEventPublisher eventPublisher, ObjectMapper objectMapper) {
    this.eventPublisher = eventPublisher;
    this.objectMapper = objectMapper;
  }

  @Override
  public void publishMessage(MessageRO message) {
    try {
      String jsonMessage = objectMapper.writeValueAsString(message);
      log.info("Message received: {}", message);
      eventPublisher.publishEvent(new MessageEvent(this, jsonMessage, messageId.getAndIncrement()));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
