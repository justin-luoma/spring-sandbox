package dev.luoma.testing.envers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface HistoryRepo extends JpaRepository<HistoryEntity, Long>, RevisionRepository<HistoryEntity, Long, Long> {
}
