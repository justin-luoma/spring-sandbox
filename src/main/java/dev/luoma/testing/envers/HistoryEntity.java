package dev.luoma.testing.envers;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Audited
public class HistoryEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  private String text;

  private Instant created = Instant.now();

  @Version
  private long version;
}
