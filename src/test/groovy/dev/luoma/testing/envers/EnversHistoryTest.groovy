package dev.luoma.testing.envers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class EnversHistoryTest extends Specification {
    @Autowired
    private HistoryRepo historyRepo;

    def "test envers"() {
        given:
        def history = new HistoryEntity()
        history.text = "testing 1"

        when:
        def saved = historyRepo.save(history)
        sleep(3000)
        saved.text = "edit 1"
        saved = historyRepo.save(saved)
        sleep(3000)
        saved.text = "edit 2"
        saved = historyRepo.save(saved)
        sleep(3000)
        saved.text = "edit 3"
        saved = historyRepo.save(saved)


        def revisions = historyRepo.findRevisions(saved.id)

        then:
        saved != null

        println("-------------------------------------------------------")
        revisions.forEach({ revision -> println(revision) })
        println("-------------------------------------------------------")
    }
}
