package dev.luoma.testing.retry.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.stubbing.Scenario
import dev.luoma.testing.common.ro.ItemRO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest
class TestServiceTest extends Specification {

    static WireMockServer wireMockServer
    public static final String URL = "/v1/testData"

    void setupSpec() {
        wireMockServer = new WireMockServer(options().port(8181))
        wireMockServer.start()
    }

    void cleanupSpec() {
        wireMockServer.stop()
    }

    @Autowired
    TestService testService

    def "test getTestData"() {
        given:
        wireMockServer.stubFor(get(urlEqualTo(URL)).inScenario("fault")
                .whenScenarioStateIs(Scenario.STARTED)
                .willReturn(aResponse()
                        .withStatus(500)
                        .withStatusMessage("Fail 1"))
                .willSetStateTo("2")
        )

        wireMockServer.stubFor(get(urlEqualTo(URL)).inScenario("fault")
                .whenScenarioStateIs("2")
                .willReturn(aResponse()
                        .withStatus(500)
                        .withStatusMessage("Fail 2"))
                .willSetStateTo("Final")
        )

        wireMockServer.stubFor(get(urlEqualTo(URL)).inScenario("fault")
                .whenScenarioStateIs("Final")
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody(jsonBody()))
        )

        when:
        def resp = testService.getTestData()
        def data = resp.get()

        then:
        data != null
    }

    String jsonBody() {
        def mapper = new ObjectMapper();
        def data = new ItemRO(1, "data")

        mapper.writeValueAsString(data)
    }
}
