package dev.luoma.testing.common.controller

import dev.luoma.testing.common.TestDataController
import dev.luoma.testing.events.MessageAdapterListener
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class TestDataControllerTest extends Specification {
    def messageAdapterListener = Mock(MessageAdapterListener)

    TestDataController testDataController = new TestDataController(messageAdapterListener)

    def "GetTestData"() {
        given:
        when: "get test data is requested"
        def response = testDataController.getTestData()

        then: "items are returned"
        def testData = response.getBody();
        1 == testData.get(0).id
    }
}
